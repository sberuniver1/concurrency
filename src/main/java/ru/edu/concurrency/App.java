package ru.edu.concurrency;

import java.time.LocalTime;
import java.util.concurrent.*;

public class App {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Runnable task = () -> {
            System.out.println("[" + LocalTime.now() + "] Custom task started thread=" + getThreadInfo());
            System.out.println("[" + LocalTime.now() + "] Custom task finished thread=" + getThreadInfo());
        };

        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(4);
        System.out.println("[" + LocalTime.now() + "] Main thread started thread=" + getThreadInfo());
        // запуск задачи через 3 секунды с периодичностью в 10 секунд
        executorService.scheduleWithFixedDelay(task, 3, 10, TimeUnit.SECONDS);
        System.out.println("[" + LocalTime.now() + "] Main thread finished thread=" + getThreadInfo());
    }

    private static String getThreadInfo() {
        // выведем информацию о текущем потоке (главном потоке)
        Thread thread = Thread.currentThread();
        // получим ID потока
        long id = thread.getId();
        // получим имя потока
        String name = thread.getName();
        return "Thread{id=" + id + ", name=" + name + "}";
    }
}
