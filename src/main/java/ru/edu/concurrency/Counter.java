package ru.edu.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class Counter {
    private AtomicInteger digit = new AtomicInteger(0);

    public void increment() {
        // атомарный метод
        digit.incrementAndGet();
    }

    public int getDigit() {
        return digit.get();
    }

    @Override
    public String toString() {
        return "Counter{" +
                "digit=" + digit +
                '}';
    }
}
