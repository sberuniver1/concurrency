package ru.edu.concurrency;

public class MyThread extends Thread {
    private final Object monitor;

    // конструктор, принимающий объект монитора
    public MyThread(Object monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {
        System.out.println("MyThread started thread=" + getThreadInfo());
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
        }
        System.out.println("MyThread finished thread=" + getThreadInfo());

        // после того, как дочерний поток выполнится, нужно оповестить об этом основной поток, чтобы он проснулся
        synchronized (monitor) {
            monitor.notify();
        }
    }

    private String getThreadInfo() {
        // выведем информацию о текущем потоке (главном потоке)
        Thread thread = Thread.currentThread();
        // получим ID потока
        long id = thread.getId();
        // получим имя потока
        String name = thread.getName();
        return "Thread{id=" + id + ", name=" + name + "}";
    }
}
